import * as React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Picker,
  Image,
  CheckBox, Alert
} from 'react-native';

import { Appbar, TextInput, Button, Card, Switch, RadioButton } from 'react-native-paper';


export default class App extends React.Component {
  state = {
    name: '',
    email: '',
    password: '',
    address: '',
    type: '',
    Breakfast: false,
    Lunch: false,
    Dinner: false,
    timing:'',
    
    isSwitchOn: false,
    active: '',
  };
  handlename = (text) => {
    this.setState({ name: text })
  };
  handleemail = (text) => {
    this.setState({ email: text })
  };
  handlepassword = (text) => {
    this.setState({ password: text })
  };
  handleaddress = (text) => {
    this.setState({ address: text })
  };
  handletype = (text) => {
    this.setState({ type: text })
  };
  handlechecked = (text) => {
    this.setState({ type: text })
  };
  handlechecked1 = (text) => {
    this.setState({ type: text })
  };
  handlechecked2 = (text) => {
    this.setState({ type: text })
  };
  handleradio1 = (text) => {
    this.setState({ type: text })
  };
  handleisSwitchOn = (text) => {
    this.setState({ type: text })
  };
  
  render() {
    const { isSwitchOn, Breakfast, Lunch, Dinner } = this.state;



    return (
      <View>
        <Appbar.Header style={styles.appbar}>

          <Appbar.Content
            title="ORDER YOUR MEAL"
            subtitle="1st Online Food Registration"
          />
          <Image style={{ width: 80,height:70 }}
            source={require('/ReactNative/FirstApp/11.png')}
          />

        </Appbar.Header>

        <ScrollView>


          <Image style={{ width: 400 }}
            source={require('/ReactNative/FirstApp/1.jpg')}
          />




          <Card style={styles.card1}>
            <Card.Content>
              <Text style={styles.check}>Username:</Text>
              <TextInput style={styles.input}
                // label='Username'
                mode='outlined'
                value={this.state.name}
                placeholder='Enter your name'
                onChangeText={this.handlename}
              />

              <Text></Text>
              <Text style={styles.check}>Email-ID:</Text>
              <TextInput
                //label='Email ID'
                mode='outlined'
                placeholder='Enter a valid email-ID'
                value={this.state.email}
                onChangeText={this.handleemail}
              />
              <Text></Text>
              <Text style={styles.check}>Password:</Text>

              <TextInput
                // label='Password'
                mode='outlined'
                secureTextEntry={true}
                placeholder='Enter the password'
                value={this.state.password}
                onChangeText={this.handlepassword}
              />
              <Text></Text>
              <Text style={styles.check}>Address:</Text>
              <TextInput
                //label='Email ID'
                mode='outlined'
                placeholder='Enter your residential address'

                value={this.state.address}
                onChangeText={this.handleaddress}
              />

              <Text></Text>
              <Text style={styles.check}>Type:</Text>


              <Picker
                mode='dropdown'
                selectedValue={this.state.type}
                style={{ height: 50, width: 150 }}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({ type: itemValue })
                }>

                <Picker.Item label="Veg" value="Veg" />
                <Picker.Item label="NonVeg" value="Nonveg" />
              </Picker>
              <Text></Text>


              <Text style={styles.check}>Time Arrival:</Text>
              <Text></Text>

              <View style={{ flexDirection: "row" ,alignItems:'center'}}>
                <CheckBox

                  Breakfast={this.state.Breakfast}
                  value={Breakfast}
                  status={Breakfast ? 'checked' : 'unchecked'}
                  onValueChange={() => { this.setState({ Breakfast: !Breakfast }); }}


                />

                <Text style={styles.check}>Breakfast</Text>
              </View>

              <View style={{ flexDirection: "row",alignItems:'center' }}>


                <CheckBox

                  Lunch={this.state.Lunch}
                  value={Lunch}
                  status={Lunch ? 'checked' : 'unchecked'}
                  onValueChange={() => { this.setState({ Lunch: !Lunch }); }}


                />

                <Text style={styles.check}>Lunch</Text>
              </View>

              <View style={{ flexDirection: "row" ,alignItems:'center'}}>
                <CheckBox

                  Dinner={this.state.Dinner}
                  value={Dinner}
                  status={Dinner ? 'checked' : 'unchecked'}
                  onValueChange={() => { this.setState({ Dinner: !Dinner }); }}


                />

                <Text style={styles.check}>Dinner</Text>
              </View>


              <Text></Text>
              <Text style={{ fontSize: 15, color: 'black' }}>Timings expected:</Text>
              <Text></Text>

              <RadioButton.Group
                onValueChange={value => this.setState({ timing:value })}
                value={this.state.timing}
              >
                <View style = {{flexDirection: 'row', alignItems: 'center'}}>
                  <RadioButton value="7 am" />
                  <Text style={{ fontSize: 15, color: 'black' }}>7.00 A.M - 11.00 A.M</Text>
                </View>
                <View style = {{flexDirection: 'row', alignItems: 'center'}}>
                  <RadioButton value="1 pm" />
                  <Text style={{ fontSize: 15, color: 'black' }}>1.00 P.M - 4.00 P.M</Text>
                </View>
                <View style = {{flexDirection: 'row', alignItems: 'center'}}>
                  <RadioButton value="6 pm" />
                  <Text style={{ fontSize: 15, color: 'black' }}>6.00 P.M - 12.00 P.M</Text>
                </View>
              </RadioButton.Group>







              <Text></Text>
              <View style={{ flexDirection: 'row' }} >

                <Text style={{ fontSize: 17, color: 'black' }}>Home Delievery: YES/NO</Text>
                <Switch style={{ alignContent: 'center' }}

                  value={isSwitchOn}
                  onValueChange={() => { this.setState({ isSwitchOn: !isSwitchOn }); }
                  }
                />


              </View>
              <Text></Text>


              <View style={{ flex: 10 }}>
                <Button style={styles.button}
                  onPress={() => fetch('http://172.17.5.120:3000/praveen', {
                    method: 'POST',
                    headers: {
                      Accept:'application/json',
                      'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                      name: this.state.name,
                      email: this.state.email,
                      password: this.state.password,
                      address:this.state.address,
                      isSwitchOn: this.state.isSwitchOn,
                      type: this.state.type,

                      Breakfast: this.state.Breakfast,
                      Lunch: this.state.Lunch,
                      Dinner: this.state.Dinner,
                      isSwitchOn: this.state.isSwitchOn,
                      timing: this.state.timing,
                    })
                  })
                  .then(res1=>Alert.alert("Table Reserved Successfully"))
                  .catch(err=>console.warn(err))
                  }>
                    <Text style={{color:'black'}}>ORDER</Text></Button>
              </View>
              <Text></Text>
              <Text></Text>
              <Text></Text>
              <Text></Text>
              


            </Card.Content>

          </Card>
          <Card>

          </Card>
        </ScrollView>

      </View >
    )
  }
}

const styles = StyleSheet.create({
  appbar: {
    backgroundColor: 'orange',
    padding: 35,
    position: 'relative',
  },
  container: {
    borderRadius: 7,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    alignSelf: 'auto',
    justifyContent: 'center',
    flex: 1,
  },
  card1: {
    justifyContent: 'space-evenly',
    marginTop: 10,

  },
  input: {
    color: 'black',
  },
  button: {
    backgroundColor: 'darkorange',
    color: 'black',
  },
  check: {
    fontSize: 15,
    color: 'black',
  }
});